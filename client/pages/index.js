import Link from 'next/dist/client/link'
import Layout from '@/components/Layout'
import EventItem from '@/components/EventItem'
import {API_URL} from '@/config/index'

export default function HomePage({events}) {
  return (
    <Layout>
      <h1>Upcoming events</h1>
      {events.length === 0 &&<h3>No events to show</h3>}

      {events.map(evt => (
        <EventItem key={evt.id} evt={evt} />
      ))}

      {events.length > 0 && (
        <Link href='/events'>
          <a className="btn-secondary">
            View All Events
          </a>
        </Link>
      )}
    </Layout>
  )
}

// Essa função precisa ter esse nome. Serve para carregar antes da renderização.

export async function getServerSideProps () {
  const res = await fetch(`${API_URL}/events?_sort=date:ASC&_limit=3`)
  const events = await res.json({})

  return {
    props: {
      events: events
    }
  }
}

// Build time para static pages. Não conseguiria pegar updates no banco de dados, só serviria uma "foto"

// export async function getStaticProps () {
//   const res = await fetch(`${API_URL}/api/events`)
//   const events = await res.json({})

//   return {
//     props: {events},
//     revalidate: 1// Carregar de novo a cada segundo
//   }
// }