import Layout from "@/components/Layout"

export default function AboutPage() {
  return (
    <Layout title='About DJ events'>
      <h1>About</h1>
      <p>
        Cool app
      </p>
      <p>
        Version: 1.0.0
      </p>
    </Layout>
  )
}
